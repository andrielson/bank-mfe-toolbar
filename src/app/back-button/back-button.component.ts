import { Component } from "@angular/core";

@Component({
  selector: "bank-mfe-toolbar-back-button",
  template: `<button type="button">Voltar</button>`,
  styleUrls: ["./back-button.component.scss"],
})
export class BackButtonComponent {}
