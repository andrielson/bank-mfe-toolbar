import { Component } from "@angular/core";
import { fromEvent } from "rxjs";
import { map, startWith } from "rxjs/operators";

@Component({
  selector: "bank-mfe-toolbar-toolbar",
  template: `
    <div id="toolbar">
      <ng-container *ngIf="showBackButton$ | async">
        <bank-mfe-toolbar-back-button></bank-mfe-toolbar-back-button>
      </ng-container>
      <bank-mfe-toolbar-title></bank-mfe-toolbar-title>
      <ng-container *ngIf="showExitButton$ | async">
        <bank-mfe-toolbar-exit-button></bank-mfe-toolbar-exit-button>
      </ng-container>
    </div>
  `,
  styleUrls: ["./toolbar.component.scss"],
})
export class ToolbarComponent {
  readonly showBackButton$ = fromEvent<CustomEvent<boolean>>(
    window,
    "bank-mfe-toolbar-show-back"
  ).pipe(
    map((event) => event.detail),
    startWith(true)
  );

  readonly showExitButton$ = fromEvent<CustomEvent<boolean>>(
    window,
    "bank-mfe-toolbar-show-exit"
  ).pipe(
    map((event) => event.detail),
    startWith(true)
  );
}
