import { Component, OnDestroy } from "@angular/core";
import { fromEvent } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { singleSpaPropsSubject } from "src/single-spa/single-spa-props";

@Component({
  selector: "bank-mfe-toolbar-root",
  template: `
    <ng-container *ngIf="showToolbar$ | async">
      <bank-mfe-toolbar-toolbar></bank-mfe-toolbar-toolbar>
    </ng-container>
  `,
})
export class AppComponent implements OnDestroy {
  readonly showToolbar$ = fromEvent<CustomEvent<boolean>>(
    window,
    "bank-mfe-toolbar-show-toolbar"
  ).pipe(
    map((event) => event.detail),
    startWith(false)
  );

  private readonly subscription = singleSpaPropsSubject.subscribe((props) =>
    console.log({ props })
  );

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
