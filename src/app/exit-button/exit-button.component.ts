import { Component } from "@angular/core";

@Component({
  selector: "bank-mfe-toolbar-exit-button",
  template: `<button type="button">Sair</button>`,
  styleUrls: ["./exit-button.component.scss"],
})
export class ExitButtonComponent {}
