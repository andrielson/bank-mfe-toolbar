import { Component } from "@angular/core";
import { fromEvent } from "rxjs";
import { map, startWith } from "rxjs/operators";

@Component({
  selector: "bank-mfe-toolbar-title",
  template: `<span>{{ title$ | async }}</span>`,
  styleUrls: ["./title.component.scss"],
})
export class TitleComponent {
  readonly title$ = fromEvent<CustomEvent<string>>(
    window,
    "bank-mfe-toolbar-title-event"
  ).pipe(
    map((event) => event.detail),
    startWith("Bank")
  );
}
