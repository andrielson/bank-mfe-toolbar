import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { BackButtonComponent } from "./back-button/back-button.component";
import { ExitButtonComponent } from "./exit-button/exit-button.component";
import { TitleComponent } from "./title/title.component";
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    BackButtonComponent,
    ExitButtonComponent,
    TitleComponent,
    ToolbarComponent,
  ],
  imports: [BrowserModule, BrowserAnimationsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
